package com.txtme.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.HttpServer;

import com.txtme.server.handler.MessageHandler;
import com.txtme.server.handler.MessagesBulkHandler;

public class ServerApplication {

    private static final int SERVER_PORT = 8081;

    public static void main(String[] args) throws IOException {

        var server = HttpServer.create(new InetSocketAddress("localhost", SERVER_PORT), 0);

        var executor = Executors.newFixedThreadPool(10);

        server.createContext("/", new MessageHandler());
        server.createContext("/bulk", new MessagesBulkHandler());
        server.setExecutor(executor);
        server.start();

        System.out.println("Server started on port " + SERVER_PORT);
    }
}
