package com.txtme.server.handler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class MessageHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {

            if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
                System.out.println("Recieved GET request");
                exchange.sendResponseHeaders(200, 0);
            } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
                var body = new String(exchange.getRequestBody().readAllBytes(), StandardCharsets.UTF_8);
                System.out.println("Recieved message: " + body);
                exchange.sendResponseHeaders(200, 0);
            } else {
                throw new BadRequestException("Unsupported method " + exchange.getRequestMethod());
            }

        } catch (Throwable t) {
            System.out.println("ERR Some problems " + t.getMessage());
            exchange.sendResponseHeaders(503, 0);
        } finally {
            var responseStream = exchange.getResponseBody();
            responseStream.close();
        }

    }

}
