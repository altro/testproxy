# Test Proxy

To start proxy service run `./gradlew proxy:run`. Proxy application will listen port **8080**.

To start server service run `./gradlew server:run`. Server application will listen port **8081**. 
