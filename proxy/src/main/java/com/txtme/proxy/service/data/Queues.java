package com.txtme.proxy.service.data;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntBinaryOperator;

public class Queues<E> {

    private static final IntBinaryOperator NEXT_INDEX_FUNCT = (index, size) -> (index + 1) % size;

    private AtomicInteger curIndex = new AtomicInteger();

    //    private MessagesLinkedList<E>[] linkedLists;
    private MessagesArrayList<E>[] arrrayLists;
    private int arrayListSize;
    private int listsCount;

    private LinkedList<MessagesArrayList<E>> completedArrayLists = new LinkedList<>();

    @SuppressWarnings("unchecked")
    public Queues(int listsCount, int arrayListSize) {

        this.listsCount = listsCount;
        this.arrayListSize = arrayListSize;

//        linkedLists = new MessagesLinkedList[listsCount];
        arrrayLists = new MessagesArrayList[listsCount];
        for (int i = 0; i < listsCount; i++) {
            //linkedLists[i] = new MessagesLinkedList<>();
            arrrayLists[i] = new MessagesArrayList<>(arrayListSize);
        }
    }

    /**
     * For implementation with linkedList
     */
/*
    public void addToLinkedList(E element) {
        synchronized (this) {
            var queue = linkedLists[curIndex.getAndAccumulate(listsCount, NEXT_INDEX_FUNCT)];
            queue.add(element);
        }
    }

    public int getLinkedTotalSize() {
        var totalSize = 0;
        for (var list : linkedLists) {
            totalSize += list.size();
        }

        return totalSize;
    }
*/

    /**
     * For implementation with arrayList
     */
    public void addToArrayList(E element) {
        synchronized (this) {
            // in real life we don't need several arrayLists, we can use only one
            var index = 0;//curIndex.getAndAccumulate(listsCount, NEXT_INDEX_FUNCT);
            var queue = arrrayLists[index];
            var added = queue.add(element);
            if (!added) {
                completedArrayLists.add(queue);
                arrrayLists[index] = new MessagesArrayList<>(arrayListSize, element);
            }
        }
    }

    public LinkedList<MessagesArrayList<E>> getCompletedArrayLists() {
        return completedArrayLists;
    }

    /**
     * This method is used if we haven't completedLists, but want to process messages.
     * For that, complete one of the non-empty lists.
     */
    public MessagesArrayList<E> completeArrayList() {
        synchronized (this) {

            MessagesArrayList<E> notEmptyArrraylist = null;
            int notEmptyArrrayListIndex = -1;

            for (int i = 0; i < arrrayLists.length; i++) {
                var arrayList = arrrayLists[i];
                if (arrayList.size() <= 0) {
                    continue;
                }
                notEmptyArrraylist = arrayList;
                notEmptyArrrayListIndex = i;
            }

            if (notEmptyArrraylist != null) {
                completedArrayLists.add(notEmptyArrraylist);
                var newQueue = new MessagesArrayList<E>(arrayListSize);
                arrrayLists[notEmptyArrrayListIndex] = newQueue;
            }

            return notEmptyArrraylist;
        }
    }

    public int getArrrayTotalSize() {
        var totalSize = 0;
        for (var list : completedArrayLists) {
            totalSize += list.size();
        }
        for (var list : arrrayLists) {
            totalSize += list.size();
        }

        return totalSize;
    }

}
