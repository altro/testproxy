package com.txtme.proxy.service;

import java.util.concurrent.CompletableFuture;

public class ProxyServcie {

    private static ProxyServcie instance;

    public static ProxyServcie get() {
        if (instance == null) {
            instance = new ProxyServcie(FailedRequestService.get(), ServerClient.get());
        }

        return instance;
    }

    private final FailedRequestService failedRequestService;
    private final ServerClient serverClient;

    public ProxyServcie(FailedRequestService failedRequestService, ServerClient serverClient) {
        this.failedRequestService = failedRequestService;
        this.serverClient = serverClient;
    }

    public void sendToServer(byte[] bodyBytes) {
        CompletableFuture.runAsync(() -> {
            var isSent = serverClient.sendToServer(bodyBytes);
            if (isSent) {
                return;
            }

            System.out.println("Can't send request to server. Add it to queue");

            failedRequestService.addMessageToQueue(bodyBytes);
        });
    }
}
