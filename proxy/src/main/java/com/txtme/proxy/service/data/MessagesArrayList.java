package com.txtme.proxy.service.data;

public class MessagesArrayList<E> {

    private int maxSize;
    private int position;
    private Object[] array;

    public MessagesArrayList(int maxSize) {
        this.maxSize = maxSize;
        array = new Object[maxSize];
//        position = new AtomicInteger(0);
    }

    public MessagesArrayList(int maxSize, E first) {
        this.maxSize = maxSize;

        array = new Object[maxSize];
        array[0] = first;
        position = 1;
//        position = new AtomicInteger(1);
    }

    public boolean add(E e) {
        // we can remove synchrpnized block here,
        // but to ensure that there are no collisions need it
//        synchronized (this) {
//        var curPos = position.get();
        if (position >= maxSize) {
            return false;
        }
        array[position] = e;
        position++;//.incrementAndGet();
//        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public E get(int i) {
        return (E) array[i];
    }

    public int size() {
        return position;
    }
}
