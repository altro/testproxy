package com.txtme.proxy.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.txtme.proxy.service.data.MessagesArrayList;
import com.txtme.proxy.service.data.Queues;

public class FailedRequestService {

    private static FailedRequestService instance;

    public static FailedRequestService get() {
        if (instance == null) {
            instance = new FailedRequestService(ServerClient.get());
        }

        return instance;
    }

    // in real life we don't need several arrayLists, we can use only one
    private final Queues<byte[]> queues = new Queues<>(1, 1000);
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final ServerClient serverClient;

    public FailedRequestService(ServerClient serverClient) {

        this.serverClient = serverClient;

        scheduler.scheduleWithFixedDelay(this::processFailedRequests, 0, 30, TimeUnit.SECONDS);
    }

    public void addMessageToQueue(byte[] message) {
        queues.addToArrayList(message);
    }

    public void processFailedRequests() {

        System.out.println("Start process failed requests");

        if (queues.getArrrayTotalSize() <= 0) {
            System.out.println("Failed requests not found");
            return;
        }

        for (; ; ) {

            MessagesArrayList<byte[]> completedList;
            var completedLists = queues.getCompletedArrayLists();
            if (completedLists == null || completedLists.isEmpty()) {
                completedList = queues.completeArrayList();
            } else {
                completedList = completedLists.getFirst();
            }

            if (completedList == null) {
                System.out.println("All queues processed");
                break;
            }

            try {
                var bulkMessage = combineMessages(completedList);

                System.out.println("Trying to resend messages with bulk request");

                var isSent = serverClient.sendBulkToServer(bulkMessage);
                if (isSent) {
                    System.out.println("Messages resent successfully");
                    completedLists.removeFirst();
                } else {
                    System.out.println("Server not respond");
                    break;
                }

            } catch (IOException e) {
                System.out.println("ERR Can't combine message before bulk request: " + e.getMessage());
                break;
            }
        }

    }

    private byte[] combineMessages(MessagesArrayList<byte[]> messages) throws IOException {

        var out = new ByteArrayOutputStream();
        for (int i = 0; i < messages.size(); i++) {
            if (i > 0) {
                out.write("\n".getBytes());
            }
            out.write(messages.get(i));
        }

        return out.toByteArray();
    }
}
