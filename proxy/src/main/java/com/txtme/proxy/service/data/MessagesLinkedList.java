package com.txtme.proxy.service.data;

public class MessagesLinkedList<E> {

    private int size;
    private Node<E> first;
    private Node<E> last;

    public void add(E e) {
//        synchronized (this) {
            final var l = last;
            final var newNode = new Node<>(l, e, null);
            last = newNode;
            if (l == null) {
                first = newNode;
            } else {
                l.next = newNode;
            }
            size++;
//        }
    }

    public int size() {
        return size;
    }

    private static class Node<E> {

        private E item;
        private Node<E> prev;
        private Node<E> next;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }

    }
}
