package com.txtme.proxy.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.Executors;

public class ServerClient {

    private static ServerClient instance;

    public static ServerClient get() {
        if (instance == null) {
            instance = new ServerClient();
        }

        return instance;
    }

    private final HttpClient httpClient;

    public ServerClient() {

        httpClient = HttpClient.newBuilder()
                .executor(Executors.newFixedThreadPool(10))
                .build();
    }

    public boolean sendToServer(byte[] messageInBytes) {

        try {

            var request = HttpRequest.newBuilder(new URI("http://localhost:8081/"))
                    .timeout(Duration.ofSeconds(1))
                    .POST(HttpRequest.BodyPublishers.ofByteArray(messageInBytes))
                    .build();

            var response = httpClient.send(request, HttpResponse.BodyHandlers.discarding());
            if (response.statusCode() == 200) {
                return true;
            }
        } catch (Throwable e) {
            System.out.println("Error with sending request: " + e.getMessage());
        }
        return false;
    }

    public boolean sendBulkToServer(byte[] messagesInBytes) {

        try {

            var request = HttpRequest.newBuilder(new URI("http://localhost:8081/bulk"))
                    .timeout(Duration.ofSeconds(10))
                    .POST(HttpRequest.BodyPublishers.ofByteArray(messagesInBytes))
                    .build();

            var response = httpClient.send(request, HttpResponse.BodyHandlers.discarding());
            // server unavailable
            if (response.statusCode() == 200) {
                return true;
            }
        } catch (Throwable e) {
            System.out.println("Error with sending request: " + e.getMessage());
        }
        return false;
    }
}
