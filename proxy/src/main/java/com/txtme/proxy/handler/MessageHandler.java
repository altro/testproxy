package com.txtme.proxy.handler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import com.txtme.proxy.service.ProxyServcie;

public class MessageHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {

            if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
                System.out.println("Recieved GET request");
                exchange.sendResponseHeaders(200, 0);
            } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
                var bodyBytes = exchange.getRequestBody().readAllBytes();
                var body = new String(bodyBytes, StandardCharsets.UTF_8);
                System.out.println("Recieved message: " + body);
                ProxyServcie.get().sendToServer(bodyBytes);
                exchange.sendResponseHeaders(200, 0);
            } else {
                throw new BadRequestException("Unsupported method " + exchange.getRequestMethod());
            }

        } catch (Throwable t) {
            System.out.println("ERR Some problems " + t.getMessage());
            exchange.sendResponseHeaders(503, 0);
        } finally {
            var responseStream = exchange.getResponseBody();
            responseStream.close();
        }

    }
}
