package com.txtme.proxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.HttpServer;

import com.txtme.proxy.handler.MessageHandler;

public class ProxyApplication {

    private static final int PROXY_PORT = 8080;

    public static void main(String[] args) throws IOException {

        var server = HttpServer.create(new InetSocketAddress("localhost", PROXY_PORT), 0);

        server.createContext("/", new MessageHandler());
        server.setExecutor(Executors.newFixedThreadPool(10));
        server.start();

        System.out.println("Server started on port " + PROXY_PORT);
    }
}
