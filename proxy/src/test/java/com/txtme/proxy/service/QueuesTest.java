package com.txtme.proxy.service;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.txtme.proxy.service.data.MessagesLinkedList;
import com.txtme.proxy.service.data.Queues;

class QueuesTest {

    AtomicLong counterTime = new AtomicLong();
    AtomicInteger counter = new AtomicInteger();
    HttpClient httpClient;

    Queues<byte[]> queues = new Queues<>(200, 10000);
    ConcurrentLinkedDeque<byte[]> deque = new ConcurrentLinkedDeque<>();
    CopyOnWriteArrayList<byte[]> copyList = new CopyOnWriteArrayList<>();
    MessagesLinkedList<byte[]> messagesLinkedList = new MessagesLinkedList<>();
    ConcurrentLinkedQueue<byte[]> linkedQueue = new ConcurrentLinkedQueue<>();

    @BeforeEach
    void setUp() {

        httpClient = HttpClient.newBuilder()
                .executor(Executors.newFixedThreadPool(10))
                .build();
    }

    @Test
    void test() throws InterruptedException, ExecutionException {

        int threadsCount = 500;

        var testMessage = "test".getBytes();

        var startTime = System.nanoTime();
        List<Callable<Object>> tasks = new ArrayList<>(threadsCount);
        for (int i = 0; i < threadsCount; i++) {
            tasks.add(callable(testMessage));
            tasks.add(callable(testMessage));
            tasks.add(callable(testMessage));
        }

        var executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        executorService.invokeAll(tasks)
                .get(threadsCount - 1)
                .get();

        System.out.println("Time: " + (System.nanoTime() - startTime) / 1000000);

        var completedLists = queues.getCompletedArrayLists();
        System.out.println("Completed: " + queues.getCompletedArrayLists().size()/* + ", totalSize: " + queues.getLinkedTotalSize()*/ + ", " + queues.getArrrayTotalSize());
    }

    private Callable<Object> callable(byte[] messageInBytes) {
        return Executors.callable(() -> {
            for (int i = 0; i < 50000; i++) {
//                linkedQueue.add(messageInBytes);
//                copyList.add(messageInBytes);
//                deque.add(messageInBytes);
//                queues.addToLinkedList(messageInBytes);
                queues.addToArrayList(messageInBytes);
            }
        });
    }
}